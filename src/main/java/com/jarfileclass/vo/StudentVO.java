package com.jarfileclass.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentVO {
	private String icno;
	private String nama;
	private String alamat;
	
	public void onPapar() {
		System.out.println("Hello World");
	}
}
